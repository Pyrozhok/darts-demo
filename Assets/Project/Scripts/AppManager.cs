﻿using UnityEngine;
using DG.Tweening;
using TMPro;
using System;

namespace Pyrog.Darts
{
	public class AppManager : MonoBehaviour
	{
		#region Simple Singletone

		private static AppManager _instance;
		public static AppManager Instance
		{
			get { return _instance; }
		}

		private void Awake()
		{
			if (_instance != null && _instance != this)
			{
				//Debug.Log("Destroyed");
				Destroy(this.gameObject);
			}
			else
			{
				_instance = this;
				Debug.Log("Init");
				_instance.Init();
			}
		}

		#endregion

		[Header("UI Managment")]
		[SerializeField] private Transform playButton;
		[SerializeField] private TextMeshProUGUI buttonLabel;
		[SerializeField] private TextMeshProUGUI scoreLabel;
		[SerializeField] private TextMeshProUGUI bestScoreLabel;
		[SerializeField] private TextMeshProUGUI arrowsLeft;

		[Header("References")]
		[SerializeField] private PowerBar powerBar;
		[SerializeField] private ArrowsSpawner spawner;
		[SerializeField] private AudioManager audioManager;
		[Header("Effects")]
		[SerializeField] private GameObject arrowHitEffect;
		[SerializeField] private GameObject winEffect;

		private const string saveKey = "bestScore";
		private const int hitsTillRestart = 10;
		private ParabolaController _lastarrow;
		private int _score = 0, _hits = 0;
		private bool isShootingAlowed;
		private AimData _lastTarget;

		private void OnEnable()
		{

		}

		private void OnDisable()
		{
			if (_lastarrow != null)
			{
				_lastarrow.OnHitTarget -= ScoreTarget;
			}
		}

		public void Init()
		{
			DontDestroyOnLoad(gameObject);
			Debug.Log("AppManager_Init completed " + Application.persistentDataPath);
			bestScoreLabel.text = PlayerPrefs.GetInt(saveKey, 0).ToString();
		}

		public void OnButtonPress()
		{
			audioManager.PlayClick();
			FlipTheButton();
			if (isShootingAlowed)
				ShootAction();
			else
				StartAction();
		}

		private void FlipTheButton(bool changeLabel = true)
		{
			playButton.DOKill(true);
			var rotation = new Vector3(-360, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.y);
			playButton.DORotate(rotation, 1f, RotateMode.LocalAxisAdd);
			DOVirtual.DelayedCall(0.5f, () =>
			{
				isShootingAlowed = !isShootingAlowed;
				buttonLabel.text = isShootingAlowed ? "SHOOT" : "START";
			});
		}

		private void StartAction()
		{
			CheckEndOfSet();
			FlipTheButton();
			_lastarrow = spawner.GetArrow();
			_lastarrow.SetStartingPosition();
			powerBar.StartMoving();
			isShootingAlowed = true;
		}

		private void CheckEndOfSet()
		{
			if (_hits >= hitsTillRestart)
			{
				spawner.Reset();
				var bestScore = PlayerPrefs.GetInt(saveKey, 0);
				if (_score > bestScore)
				{
					PlayerPrefs.SetInt(saveKey, _score);
				}
				_hits = 0;
				_score = 0;
				bestScoreLabel.text = PlayerPrefs.GetInt(saveKey, 0).ToString();
				arrowsLeft.text = "x" + (hitsTillRestart - _hits);
			}
		}

		private void ShootAction()
		{
			powerBar.StopMoving();
			_lastTarget = powerBar.GetTarget();
			spawner.CorrectTargetPositions(_lastTarget);
			_lastarrow.OnHitTarget += ScoreTarget;
			_lastarrow.OnHitTarget += OnTargetHitted;
			_lastarrow.FollowParabola();
			audioManager.PlayShoot();
		}

		private void ScoreTarget()
		{
			_hits++;
			arrowsLeft.text = "x" + (hitsTillRestart - _hits);
			_score += GetScore(_lastTarget.score);
			scoreLabel.text = _score.ToString();
			powerBar.BumpTheSpeed();
			_lastarrow.OnHitTarget -= ScoreTarget;
			_lastarrow.OnHitTarget -= OnTargetHitted;
		}

		private void OnTargetHitted()
		{
			arrowHitEffect.SetActive(false);
			arrowHitEffect.SetActive(true);
			audioManager.PlaySoundEffect(_lastTarget.score != HitScore.Miss);
			if(_hits>=hitsTillRestart)
			{
				winEffect.SetActive(false);
				winEffect.SetActive(true);
			}
		}

		private int GetScore(HitScore score)
		{
			switch (score)
			{
				case HitScore.Red:
					return 50;

				case HitScore.Yellow:
					return 30;

				case HitScore.Green:
					return 20;

				case HitScore.Miss:
					return 0;
			};
			return 0;
		}
	}


}
