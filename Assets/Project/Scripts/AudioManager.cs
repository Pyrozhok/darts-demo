﻿using UnityEngine;

namespace Pyrog.Darts
{
	public class AudioManager : MonoBehaviour
	{

		[SerializeField] AudioClip musicLoop;
		[SerializeField] AudioClip clickSound;
		[SerializeField] AudioClip winSound;
		[SerializeField] AudioClip lostSound;
		[SerializeField] AudioClip shootSound;
		[SerializeField] AudioSource musicSource;
		[SerializeField] AudioSource targetEffectsSource;
		[SerializeField] AudioSource effectsSource;
		[SerializeField] AudioSource uiSoundsSource;

		void Start()
		{
			musicSource.clip = musicLoop;
			musicSource.Play();
		}

		public void PlaySoundEffect(bool isWin)
		{
			effectsSource.clip = isWin ? winSound : lostSound;
			effectsSource.Play();
		}
		public void PlayClick()
		{
			uiSoundsSource.clip = clickSound;
			uiSoundsSource.Play();
		}
		public void PlayShoot()
		{
			targetEffectsSource.clip = shootSound;
			targetEffectsSource.Play();
		}

		void Update()
		{
			if (!musicSource.isPlaying)
			{
				musicSource.Play();
			}

		}
	}
}
