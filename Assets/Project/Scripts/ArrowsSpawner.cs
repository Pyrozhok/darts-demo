﻿using System.Collections.Generic;
using UnityEngine;

namespace Pyrog.Darts
{
	public class ArrowsSpawner : MonoBehaviour
	{
		[SerializeField] private GameObject arrowPrefab;
		[SerializeField] private Transform parabolaRoot;
		[SerializeField] private Transform targetPivot;
		[SerializeField] private Transform middlePivot;
		[SerializeField] private Transform midPoint;
		[SerializeField] private Transform endPoint;
		private List<GameObject> _arrowsPool = new List<GameObject>();
		private int _maxArrowsOnTarget = 3;
	
		public ParabolaController GetArrow()
		{
			ParabolaController arrow = FirstInActive();
			if (arrow == null)
			{
				arrow = SpawnArrow();
			}
			return arrow;
		}

		private ParabolaController FirstInActive()
		{
			for (int i = 0; i < _arrowsPool.Count; i++)
			{
				if (!_arrowsPool[i].activeInHierarchy)
				{
					var controller = _arrowsPool[i].GetComponent<ParabolaController>();
					return controller;
				}
			}
			return null;
		}

		internal void CorrectTargetPositions(AimData target)
		{
			Debug.Log("Correct positions " + target.ToString());
			Vector2 circlePos = GeneratePointOnCircle(target.isLeftSide);
			float radius = GetRadiusForScore(target.score);
			Vector2 circlePosEnd = circlePos * radius;
			Vector2 circlePosMid = circlePos * radius * 2;

			targetPivot.localPosition = Vector3.zero;
			var pos = targetPivot.localPosition;
			pos.x = circlePosEnd.x;
			pos.y = circlePosEnd.y;
			targetPivot.localPosition = pos;
			if(target.score == HitScore.Miss)
			{
				var poz = targetPivot.position;
				poz.z -= 100;
				poz.y = 10;
				targetPivot.position = poz;
			}
			endPoint.position = targetPivot.position;

			pos = middlePivot.localPosition;
			pos.x = circlePosMid.x;
			pos.y = circlePosMid.y;
			middlePivot.localPosition = pos;
			midPoint.position = middlePivot.position;

		}


		#region for testing purpose
		public void Test()
		{
			for (int i = 0; i < 50; i++)
			{
				Vector3 position = GetEndPosition(GetRadiusForScore(HitScore.Green), true);
				var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
				cube.name = "endPoint " + i;
				cube.transform.localScale = Vector3.one * .25f;
				cube.transform.position = position;
			}


			for (int i = 0; i < 50; i++)
			{
				Vector3 position = GetMiddlePosition(GetRadiusForScore(HitScore.Green), true);
				var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
				cube.name = "midPoint " + i;
				cube.transform.localScale = Vector3.one * .25f;
				cube.transform.position = position;
			}
		}

		private Vector3 GetEndPosition(float radius, bool isLeft)
		{
			var pos = targetPivot.localPosition;
			Vector2 circlePos = GeneratePointOnCircle(isLeft);
			circlePos = circlePos * radius;
			pos.x = circlePos.x;
			pos.y = circlePos.y;
			targetPivot.localPosition = pos;
			return targetPivot.position;
		}
		private Vector3 GetMiddlePosition(float radius, bool isLeft)
		{
			var pos = middlePivot.localPosition;
			Vector2 circlePos = GeneratePointOnCircle(isLeft);
			circlePos = circlePos * radius*2;
			pos.x = circlePos.x;
			pos.y = circlePos.y;
			middlePivot.localPosition = pos;
			return middlePivot.position;
		}

		#endregion

		private static Vector2 GeneratePointOnCircle(bool isLeft)
		{
			int angle = isLeft ? UnityEngine.Random.Range(110, 250) : UnityEngine.Random.Range(-70, 70);
		//	int angle = UnityEngine.Random.Range(0, 180);
			float radian = angle * Mathf.Deg2Rad;
			var x = Mathf.Cos(radian);
			var y = Mathf.Sin(radian);
			var pos = new Vector2(x, y);
			return pos;
		}

		private float GetRadiusForScore(HitScore score)
		{
			switch (score)
			{
				case HitScore.Red:
					return Random.Range(0, 10);
				case HitScore.Yellow:
					return Random.Range(11, 21);
				case HitScore.Green:
					return Random.Range(22, 32);
				case HitScore.Miss:
					return Random.Range(32, 50);
			}
			return 0;
		}

		public ParabolaController SpawnArrow()
		{
			GameObject arrow = Instantiate(arrowPrefab);
			_arrowsPool.Add(arrow);
			var controller = arrow.GetComponent<ParabolaController>();
			if (controller != null)
			{
				controller.Init(parabolaRoot);
			}
			return controller;
		}

		public void Reset()
		{
			for (int i = 0; i < _arrowsPool.Count; i++)
			{
				if (_arrowsPool[i].activeInHierarchy)
				{
					_arrowsPool[i].SetActive(false);
				}
			}
		}
	}
}