﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

namespace Pyrog.Darts
{
	public enum HitScore
	{
		Red = 0,
		Yellow = 1,
		Green = 2,
		Miss = 3
	}

	public struct AimData
	{
		public HitScore score;
		public bool isLeftSide;

		public override string ToString()
		{
			return "Score = " + score.ToString() + " is Left = " + isLeftSide;
		}
	}

	public class PowerBar : MonoBehaviour
	{
		[Range(50, 150)] public float speed = 1;
		[SerializeField] private Transform pivot;
		[SerializeField] private Material dotMaterial;

		private MeshRenderer _pivotMesh;
		private const float redPersent = 0.1f;
		private const float yellowPersent = 0.26f;
		private const float greenPersent = 0.3f;
		private float _redLimit = 0, _yellowLimit = 0, _greenLimit = 0;
		private float _direction = 1;
		private float _barSize = 30;
		private bool _isMoving = false;
		private AimData _aimData;



		private void Awake()
		{
			_redLimit = _barSize * redPersent;
			_yellowLimit = _barSize * (redPersent + yellowPersent);
			_greenLimit = _barSize * (redPersent + yellowPersent + greenPersent);
			_pivotMesh = pivot.GetComponent<MeshRenderer>();
		}

		public AimData GetTarget()
		{
			return _aimData;
		}

		public void StartMoving()
		{
			_isMoving = true;
		}

		public void StopMoving()
		{
			_isMoving = false;
			UpdateTargetData();
		}

		public void BumpTheSpeed()
		{
			speed ++;
			speed = Mathf.Clamp(speed, 50, 150);

		}


		private void UpdateTargetData()
		{
			_aimData.score = GetHitScoreFromPosition();
			_aimData.isLeftSide = pivot.localPosition.x < 0;
		}

		private HitScore GetHitScoreFromPosition()
		{
			float posAbs = Mathf.Abs(pivot.localPosition.x);
			if (posAbs < _redLimit)
			{
				return HitScore.Red;
			}
			if (posAbs < _yellowLimit)
			{
				return HitScore.Yellow;
			}
			if (posAbs < _greenLimit)
			{
				return HitScore.Green;
			}
			return HitScore.Miss;
		}

		private HitScore _lastScore;
		private void Update()
		{
			if (_isMoving)
			{
				Vector3 tempPos = pivot.localPosition;
				tempPos.x += Time.deltaTime * speed * _direction;
				if (tempPos.x > _barSize || tempPos.x < -_barSize)
				{
					tempPos.x = _barSize * _direction;
					_direction *= -1;
				}
				pivot.localPosition = tempPos;
				var currentScore = GetHitScoreFromPosition();
				if (currentScore != _lastScore)
				{
					_lastScore = currentScore;
					ChangeDotColor(_lastScore);
				}
			}
		}

		private void OnDestroy()
		{
			dotMaterial.color = Color.red;
		}

		private void ChangeDotColor(HitScore score)
		{
			switch (score)
			{
				case HitScore.Red:
					dotMaterial.color = Color.red;
					break;
				case HitScore.Yellow:
					dotMaterial.color = Color.yellow;
					break;
				case HitScore.Green:
					dotMaterial.color = Color.green;
					break;
				case HitScore.Miss:
					dotMaterial.color = Color.white;
					break;

			}
		}
	}
}
